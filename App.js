import React from 'react';
import {
  StyleSheet, Text, View,
  FlatList, Image,
} from 'react-native';


import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {connect, Provider} from 'react-redux';
import {createList,} from "./store/actions/actionList";
import listReducer from "./store/reducers/listReducer";


class App extends React.Component {

  componentDidMount() {
    this.props.createList();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={{fontSize: 25,}}>List</Text>
        </View>
        <View style={styles.flatContainer}>
          <FlatList
            data={Object.keys(this.props.list).map((data) => (this.props.list[data]))}
            keyExtractor={(item) => item.data.id}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              this.props.createList();
            }}
            renderItem={({item}) => {
              return (
                <View style={styles.Cart}>
                  <Image
                    source={{uri: item.data.thumbnail}}
                    style={{width: '20%', height: '100%', marginRight: 10}}
                  />
                  <Text style={{fontSize: 13, width: '80%'}}>{item.data.title}</Text>
                </View>
              );
            }}

          />
        </View>
      </View>
    );
  }
}



const mapStateToProps = state => ({
  list: state.list,
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  createList: () => dispatch(createList()),
});

const AppConnected = connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 30
  },
  flatContainer: {
    flex: 8,
  },
  Cart: {
    padding: 10,
    marginTop: 10,
    height: 100,
    backgroundColor: '#E6E6FA',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    borderBottomColor: '#4B0082',
    borderBottomWidth: 3,
  },
});


const store = createStore(
  listReducer,
  applyMiddleware(thunkMiddleware)
);

const index = () => (
  <Provider store={store}>
    <AppConnected />
  </Provider>
);


export default index;
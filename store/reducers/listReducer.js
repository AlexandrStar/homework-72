import {
  LIST_FAILURE,
  LIST_REQUEST, LIST_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  list: [],
  loading: false,
  error: null,
  ordered: false,
};

const contactReducer = (state = initialState, action) => {
  switch (action.type) {
    case LIST_REQUEST:
      return {...state, loading: true};
    case LIST_SUCCESS:
      return {...state, loading: false, list: action.list};
    case LIST_FAILURE:
      return {...state, loading: false, error: action.error};
    default:
      return state;
  }
};

export default contactReducer;
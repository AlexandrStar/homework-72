import axios from '../../axios-order';
import {LIST_FAILURE, LIST_REQUEST, LIST_SUCCESS,} from "./actionTypes";

export const listRequest = () => ({type: LIST_REQUEST});
export const listSuccess = list => ({type: LIST_SUCCESS, list});
export const listFailure = error => ({type: LIST_FAILURE, error});

let url = 'pics.json';

export const createList = () => {
  return (dispatch, getState) => {
    dispatch(listRequest());
    const after = getState().after;
    if (after !== '') {
      url = 'pics.json?count=5&after=' + after;
    }
    axios.get(url).then(response => {
        const result = response.data;
        const children = result.data.children;
        dispatch(listSuccess(children, result.data.after));
      },
      error => dispatch(listFailure(error))
    );
  }
};
